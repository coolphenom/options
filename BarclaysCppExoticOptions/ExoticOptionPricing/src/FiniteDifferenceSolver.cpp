/*
 * FiniteDifferenceSolver.cpp
 *
 *  Created on: Jun 18, 2019
 *      Author: pavlotkachenko
 */

#include <algorithm>
#include <tr1/tuple>

#include "FiniteDifferenceSolver.h"

using namespace std;

double FiniteDifferenceSolver::m_valueToCompare;

bool FiniteDifferenceSolver::optionPriceComparatorLess(double a, double b)
{
	return min(a - m_valueToCompare, b - m_valueToCompare);
}

bool FiniteDifferenceSolver::optionPriceComparatorMore(double a, double b)
{
	return max(a - m_valueToCompare, b - m_valueToCompare);
}

size_t gsl_matrix_get_size1(gsl_matrix* m)
{
	return m->size1;
}

size_t gsl_matrix_get_size2(gsl_matrix* m)
{
	return m->size2;
}

gsl_matrix* gsl_matrix_add_separate(gsl_matrix* A, gsl_matrix* B)
{
	if (gsl_matrix_get_size1(A) == gsl_matrix_get_size1(B) &&
			gsl_matrix_get_size2(A) == gsl_matrix_get_size2(B))
	{
		size_t nr = gsl_matrix_get_size1(A);
		size_t nc = gsl_matrix_get_size2(A);
		gsl_matrix* C = gsl_matrix_alloc(nr,nc);
		for (size_t i = 0; i < nr; ++i)
		{
			for (size_t j = 0; j < nc; ++j)
			{
				gsl_matrix_set(C, i,j, gsl_matrix_get(A,i,j) + gsl_matrix_get(B,i,j));
			}
		}
		return C;
	}
	else
	{
		throw runtime_error("unequal dimensions");
	}
}

gsl_matrix* gsl_matrix_subtract_separate(gsl_matrix* A, gsl_matrix* B)
{
	if (gsl_matrix_get_size1(A) == gsl_matrix_get_size1(B) &&
			gsl_matrix_get_size2(A) == gsl_matrix_get_size2(B))
	{
		size_t nr = gsl_matrix_get_size1(A);
		size_t nc = gsl_matrix_get_size2(A);
		gsl_matrix* C = gsl_matrix_alloc(nr,nc);
		for (size_t i = 0; i < nr; ++i)
		{
			for (size_t j = 0; j < nc; ++j)
			{
				gsl_matrix_set(C, i,j, gsl_matrix_get(A,i,j) - gsl_matrix_get(B,i,j));
			}
		}
		return C;
	}
	else
	{
		throw runtime_error("unequal dimensions");
	}
}


gsl_matrix* MultiplyMatrix(gsl_matrix* A, gsl_matrix* B, const bool& deallocateA, const bool& deallocateB)
{
	size_t nrA = gsl_matrix_get_size1(A);
	size_t ncB = gsl_matrix_get_size2(B);
	gsl_matrix* resMat = gsl_matrix_alloc(nrA, ncB);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans, 1.0, A, B, 0.0, resMat);
	if (deallocateA)
		gsl_matrix_free(A);
	if (deallocateB)
		gsl_matrix_free(B);
	return resMat;
}

gsl_matrix* ExponentMatrix(gsl_matrix* A)
{
	size_t nrA = gsl_matrix_get_size1(A);
	size_t ncA = gsl_matrix_get_size2(A);
	gsl_matrix* resMat = gsl_matrix_alloc(nrA, ncA);
	for (size_t i = 0; i < nrA; ++i)
	{
		for (size_t j = 0; j < ncA; ++j)
		{
			double value = exp(gsl_matrix_get(A, i, j));
			gsl_matrix_set(resMat, i, j, value);
		}
	}
	return resMat;
}

//LU SOLVER
gsl_matrix* InverseMatrixLU(gsl_matrix* A)
{
	double tolerance = 1e-16;
	size_t size1 = gsl_matrix_get_size1(A);
	gsl_matrix* invertMe = gsl_matrix_alloc(size1, size1);
	gsl_matrix* outMat = gsl_matrix_alloc(size1,size1);
	gsl_permutation* perm = gsl_permutation_alloc(size1);
	int sign;
	gsl_matrix_memcpy(invertMe, A);
	gsl_linalg_LU_decomp(invertMe, perm, &sign);
	gsl_set_error_handler_off();
	double det = gsl_linalg_LU_det(invertMe, sign);
	if (abs(det) > tolerance)
	{
		gsl_linalg_LU_invert(invertMe,perm,outMat);
		gsl_matrix_free(invertMe);
		gsl_permutation_free(perm);
		return outMat;
	}
	else
	{
		cerr << string("Matrix is singular\n");
		throw runtime_error("singular matrix");
	}
}


gsl_matrix* DeclareGslMatrix(vector< vector<double> >& values)
{
	size_t nr = values.size();
	size_t nc = values[0].size();
	gsl_matrix* x = gsl_matrix_alloc(nr,nc);
	for (size_t i = 0; i < nr; ++i)
	{
		vector<double>& row = values[i];
		for (size_t j = 0; j < nc; ++j)
		{
			gsl_matrix_set(x,i,j,row[j]);
		}
	}
	return x;
}

vector< vector<double> > GslMatrixToVector(gsl_matrix* A, const bool& freeMemory)
{
	size_t nr = gsl_matrix_get_size1(A);
	size_t nc = gsl_matrix_get_size2(A);
	vector< vector<double> > matrixVec;
	for (size_t i = 0; i < nr; ++i)
	{
		vector<double> row;
		for (size_t j = 0; j < nc; ++j)
		{
			row.push_back(gsl_matrix_get(A, i, j));
		}
		matrixVec.push_back(row);
	}
	if (freeMemory)
		gsl_matrix_free(A);
	return matrixVec;
}

vector<double> GslColumnToVector(const size_t& nc, gsl_matrix* A)
{
	size_t length = gsl_matrix_get_size2(A);
	vector<double> vec(length);
	for (size_t i = 0; i < length; ++i)
	{
		vec[i] = gsl_matrix_get(A, i, nc);
	}
	return vec;
}

vector<double> GslColumnToVector1(const size_t& nc, gsl_matrix* A)
{
	size_t length = gsl_matrix_get_size1(A);
	vector<double> vec(length);
	for (size_t i = 0; i < length; ++i)
	{
		vec[i] = gsl_matrix_get(A, i, nc);
	}
	return vec;
}

void MatrixOutput(gsl_matrix* m)
{
	cout << "-----------------------------" << endl;
	size_t nr = gsl_matrix_get_size1(m);
	size_t nc = gsl_matrix_get_size2(m);
	for (size_t i = 0; i < nr; ++i)
	{
		for (size_t j = 0; j < nc; ++j)
		{
			cout << gsl_matrix_get(m, i, j) << " ";
		}
		cout << endl;
	}
	cout << "---------------------------" << endl;
}

FiniteDifferenceSolver::FiniteDifferenceSolver()
{
	m_invertSolverMatrix = 0;
}

//Tridiagonal solver for a system of equations Ax=b
//No stability check was provided
gsl_matrix* FiniteDifferenceSolver::TridiagonalSolver(gsl_matrix* A, gsl_matrix* coef, const size_t& indStart, const size_t& indFinish)
{
	gsl_matrix_view subA = gsl_matrix_submatrix(A, indStart, indStart, indFinish - indStart + 1, indFinish - indStart + 1);
	gsl_matrix_view subCoef = gsl_matrix_submatrix(coef, indStart, 0, indFinish - indStart + 1, 1);

	size_t size = gsl_matrix_get_size1(&subA.matrix);

	gsl_vector* dPrime = gsl_vector_alloc(size);
	gsl_vector* cPrime = gsl_vector_alloc(size);
	gsl_matrix* result = gsl_matrix_alloc(size, 1);
	for (size_t i = 0; i < size; ++i)
	{
		if (i == 0)
		{
			double valueC = gsl_matrix_get(&subA.matrix,0,1)/gsl_matrix_get(&subA.matrix, 0, 0);
			double valueD = gsl_matrix_get(&subCoef.matrix,0,0)/gsl_matrix_get(&subA.matrix, 0, 0);
			gsl_vector_set(cPrime, i, valueC);
			gsl_vector_set(dPrime, i, valueD);
		}
		else if (i > 0 && i < size - 1)
		{
			double a = gsl_matrix_get(&subA.matrix, i, i-1);
			double b = gsl_matrix_get(&subA.matrix, i, i);
			double c = gsl_matrix_get(&subA.matrix, i, i+1);
			double d = gsl_matrix_get(&subCoef.matrix, i, 0);
			double valueC = c/(b - a*gsl_vector_get(cPrime,i - 1));
			double valueD = (d - a*gsl_vector_get(dPrime, i - 1))/(b - a*gsl_vector_get(cPrime,i - 1));
			gsl_vector_set(cPrime, i, valueC);
			gsl_vector_set(dPrime, i, valueD);
		}
		else
		{
			double a = gsl_matrix_get(&subA.matrix, i, i-1);
			double b = gsl_matrix_get(&subA.matrix, i, i);
			double d = gsl_matrix_get(&subCoef.matrix, i, 0);
			double valueD = (d - a*gsl_vector_get(dPrime, i - 1))/(b - a*gsl_vector_get(cPrime,i - 1));
			gsl_vector_set(dPrime, i, valueD);
		}
	}

	for (int i = size-1; i >= 0; --i)
	{
		double value = 0;
		if (i == (int)size - 1)
		{
			value = gsl_vector_get(dPrime,size-1);
		}
		else
		{
			value = gsl_vector_get(dPrime, i) - gsl_vector_get(cPrime, i)*gsl_matrix_get(result, i + 1, 0);
		}
		gsl_matrix_set(result, i, 0, value);
	}
	gsl_vector_free(cPrime);
	gsl_vector_free(dPrime);
	return result;
}

//LU solver for a system of equations Ax=b
gsl_matrix* FiniteDifferenceSolver::LUSolver(gsl_matrix* A, gsl_matrix* b, const size_t& indStart, const size_t& indFinish)
{
	if (m_invertSolverMatrix == 0)
	{
		gsl_matrix* Ar = InverseMatrixLU(A);
		m_invertSolverMatrix = Ar;
		cout << "A_r" << endl;
		MatrixOutput(Ar);
		//MatrixOutput(b);
		size_t sizeAr = gsl_matrix_get_size1(Ar);
		gsl_matrix_view ArSub = gsl_matrix_submatrix(Ar, indStart, 0, indFinish - indStart + 1, sizeAr);
		gsl_matrix* result = MultiplyMatrix(&ArSub.matrix, b, false, false);
		return result;
		//return MultiplyMatrix(m_invertSolverMatrix, b, false, false);
	}
	else
	{
		gsl_matrix* Ar = m_invertSolverMatrix;
		size_t sizeAr = gsl_matrix_get_size1(Ar);
		gsl_matrix_view ArSub = gsl_matrix_submatrix(Ar, indStart, 0, indFinish - indStart + 1, sizeAr);
		gsl_matrix* result = MultiplyMatrix(&ArSub.matrix, b, false, false);
		return result;
	}
}

pair<gsl_matrix*, gsl_matrix*> FiniteDifferenceSolver::GenerateGrid(const size_t& n, const size_t& N, const size_t& M, const double& t,
		const double& s0, const double& K, const double& r, const bool& put)
{
	double dt = t/n;
	double ds = s0/((M-1)/2);
	vector<double> row(N,0);
	vector< vector<double> > rows(M, row);
	gsl_matrix* V = DeclareGslMatrix(rows);
	gsl_matrix* S = DeclareGslMatrix(rows);

	//asset price grid
	for (size_t j = 1; j < M; ++j)
	{
		for (size_t i = 0; i < N; ++i)
		{
			gsl_matrix_set(S, j, i, gsl_matrix_get(S, j-1, i)+ds);
		}
	}

	if (put)
	{
		for (size_t j = 0; j < M; ++j)
		{
			gsl_matrix_set(V, j, N - 1, max(K - gsl_matrix_get(S, j, N -1), (double)0));
		}

		for (int i = N - 2; i >= 0; --i)
		{
			gsl_matrix_set(V, 0, i, gsl_matrix_get(V, 0, i+1)*exp(-r*dt));
		}
	}
	else
	{
		for (size_t j = 0; j < M; ++j)
		{
			gsl_matrix_set(V, j, N - 1, max(gsl_matrix_get(S, j, N -1) - K, (double)0));
		}

		for (int i = N - 2; i >= 0; --i)
		{
			gsl_matrix_set(V, M - 1, i, gsl_matrix_get(V, M - 1, i+1)*exp(-r*dt));
		}
	}
	return make_pair(V, S);
}

gsl_matrix* FiniteDifferenceSolver::GenerateAssetGrid(const double& start, const double& finish, const size_t& n, const size_t& N, const size_t& M, const double& t,
		const double& s0, const double& K, const double& r, const bool& put)
{
	double ds = (finish-start)/(M-1);
	vector<double> row(N, start);
	vector< vector<double> > rows(M, row);
	gsl_matrix* S = DeclareGslMatrix(rows);
	//asset price grid
	for (size_t j = 1; j < M; ++j)
	{
		for (size_t i = 0; i < N; ++i)
		{
			gsl_matrix_set(S, j, i, gsl_matrix_get(S, j-1, i)+ds);
		}
	}
	return S;
}

gsl_matrix* FiniteDifferenceSolver::GenerateValueGrid(gsl_matrix* S, const size_t& n, const size_t& N, const size_t& M, const double& t,
		const double& s0, const double& K, const double& r, const double& rebate, const OptionTypes& option, const bool& put)
{
	double dt = (double)t/n;
	vector<double> row(N,0);
	vector< vector<double> > rows(M, row);
	gsl_matrix* V = DeclareGslMatrix(rows);

	OptionType type = option.GetType();
	if (put)
	{
		for (size_t j = 0; j < M; ++j)
		{
			gsl_matrix_set(V, j, N - 1, max(K - gsl_matrix_get(S, j, N -1), (double)0));
		}
	}
	else
	{
		for (size_t j = 0; j < M; ++j)
		{
			gsl_matrix_set(V, j, N - 1, max(gsl_matrix_get(S, j, N -1) - K, (double)0));
		}
	}
	//MatrixOutput(V);
	switch (type)
	{
		case KnockOut:
		{
			RenewKnockoutZone(rebate, N - 1, V, S, option, put);
		}
		break;
		default:
			break;
	}
	for (int i = N - 2; i >= 0; --i)
	{
		gsl_matrix_set(V, 0, i, gsl_matrix_get(V, 0, i+1)*exp(-r*dt));
		gsl_matrix_set(V, M - 1, i, gsl_matrix_get(V, M - 1, i+1)*exp(-r*dt));
	}
	return V;
}

gsl_matrix* FiniteDifferenceSolver::GenerateAddCoefficients(gsl_matrix* V, const size_t& N, const size_t& M,
		gsl_matrix* cf, const Scheme& scheme)
{
	vector<double> addRow(N-1,0);
	vector< vector<double> > addMatrix(M-2, addRow);
	gsl_matrix* add = DeclareGslMatrix(addMatrix);
	switch (scheme)
	{
		case CrankNicolsonIm:
		{
			CrankNicolson:
			for (int i = N-2; i >= 0; --i)
			{
				double val1 = gsl_matrix_get(cf,0,2)*(gsl_matrix_get(V,0,i) + gsl_matrix_get(V,0,i + 1));
				double val2 = gsl_matrix_get(cf,0,0)*(gsl_matrix_get(V,M-2,i) + gsl_matrix_get(V,M-2, i + 1));
				gsl_matrix_set(add, 0, i, val1);
				gsl_matrix_set(add, M-3, i, val2);
			}
		}
		break;
		case CrankNicolsonDuffyIm:
			goto CrankNicolson;
		break;
		default:
		{
			for (int i = N-2; i >= 0; --i)
			{
				double val1 = -gsl_matrix_get(cf,1,0)*gsl_matrix_get(V,0,i);
				double val2 = gsl_matrix_get(cf,M-2,2)*gsl_matrix_get(V,M-2,N-3);
				gsl_matrix_set(add, 0, i, val1);
				gsl_matrix_set(add, M-3, i, val2);
			}
		}
		break;
	}

	return add;
}

gsl_matrix* FiniteDifferenceSolver::GenerateCoefficients(gsl_matrix* S, const size_t& M, const double& t, const size_t& n,
		const double& sd, const double& r, const double& y, const Scheme& scheme)
{
	double dt = (double)t/n;
	double dT = (double)t/n;
	double dlogS = sd * sqrt(2* dT);
	vector<double> coefsLayer(3,0);
	vector< vector<double> > coefs(M, coefsLayer);
	gsl_matrix* cf = DeclareGslMatrix(coefs);
	switch (scheme)
	{
		case Implicit:
		{
			for (size_t j = 0; j < M; ++j)
			{
				double c1 = 0.5*(r-y)*((double)j)*dt - 0.5*dt*sd*sd*((double)j)*((double)j);
				double c2 = 1+sd*sd*((double)j)*((double)j)*dt+r*dt;
				double c3 = -0.5*(r-y)*((double)j)*dt - 0.5*dt*sd*sd*((double)j)*((double)j);
				gsl_matrix_set(cf,j,0, c1);
				gsl_matrix_set(cf,j,1, c2);
				gsl_matrix_set(cf,j,2, c3);
			}
		}
		break;
		case ExplicitLog:
		{
			for (size_t j = 0; j < M; ++j)
			{
				double factor = dT/(1+r*dT);
				double c1 = factor * ((sd/dlogS)*(sd/dlogS) - (r - sd*sd/2)/dlogS)*0.5;
				double c2 = factor / dT * (1 - (sd/dlogS)*(sd/dlogS)*dT);
				double c3 = factor * ((sd/dlogS)*(sd/dlogS) + (r - sd*sd/2)/dlogS)*0.5;
				gsl_matrix_set(cf,j,0, c1);
				gsl_matrix_set(cf,j,1, c2);
				gsl_matrix_set(cf,j,2, c3);
			}
		}
		break;
		case ImplicitLog:
		{
			for (size_t j = 0; j < M; ++j)
			{
				double c1 = (r - 0.5*sd*sd)*dT/(2*dlogS) - 0.5*sd*sd*dT/(dlogS*dlogS);
				double c2 = (1+ sd*sd*dT/(dlogS*dlogS) + r*dT);
				double c3 = -(r - 0.5*sd*sd)*dT/(2*dlogS) - 0.5*sd*sd*dT/(dlogS*dlogS);
				gsl_matrix_set(cf,j,0, c1);
				gsl_matrix_set(cf,j,1, c2);
				gsl_matrix_set(cf,j,2, c3);
			}
		}
		break;
		case CrankNicolsonIm:
		{
			for (size_t j = 0; j < M; ++j)
			{
				if (j == 0 || j == M-1)
				{
					gsl_matrix_set(cf, j, 0, 0);
					gsl_matrix_set(cf, j, 1, exp(dt*r));
					gsl_matrix_set(cf, j, 2, 0);
				}
				else
				{
					double c1 = 0.5*(r-y)*((double)j)*dt - dt*sd*sd*((double)j)*((double)j)/4;
					double c2 = 1+sd*sd*((double)j)*((double)j)*dt/2+r*dt/2;
					double c3 = -0.5*(r-y)*((double)j)*dt - dt*sd*sd*((double)j)*((double)j)/4;
					gsl_matrix_set(cf,j,0, c1);
					gsl_matrix_set(cf,j,1, c2);
					gsl_matrix_set(cf,j,2, c3);
				}

			}
		}
		break;
		case CrankNicolsonDuffyIm:
		{
			double ds = gsl_matrix_get(S,1,0) - gsl_matrix_get(S,0,0);
			for (size_t j = 0; j < M; ++j)
			{
				if (j == 0 || j == M-1)
				{
					gsl_matrix_set(cf, j, 0, 0);
					gsl_matrix_set(cf, j, 1, exp(dt*r));
					gsl_matrix_set(cf, j, 2, 0);
				}
				else
				{
					double mu = r*gsl_matrix_get(S,j,0);
					double sigma = 0.5* sd*sd*gsl_matrix_get(S,j,0)*gsl_matrix_get(S,j,0);
					double rho = j != 0 ? mu*ds*0.5*1/(tanh(mu*ds/(2*sigma))) : 0;
					double c1 = r*gsl_matrix_get(S,j,0)*dt/(4*ds) - rho*dt/(2*ds*ds);
					double c2 = 1 + r*dt/2 + rho * dt/(ds*ds);
					double c3 = -r*gsl_matrix_get(S,j,0)*dt/(4*ds) - rho*dt/(2*ds*ds);
					gsl_matrix_set(cf,j,0, c1);
					gsl_matrix_set(cf,j,1, c2);
					gsl_matrix_set(cf,j,2, c3);
				}
			}
		}
		break;
		case DuffyIm:
		{
			double ds = gsl_matrix_get(S,1,0) - gsl_matrix_get(S,0,0);
			for (size_t j = 0; j < M; ++j)
			{
				double mu = r*gsl_matrix_get(S,j,0);
				double sigma = 0.5* sd*sd*gsl_matrix_get(S,j,0)*gsl_matrix_get(S,j,0);
				double rho = j != 0 ? mu*ds*0.5*1/(tanh(mu*ds/(2*sigma))) : 0;
				if (rho < 0.01)
					rho = mu*ds/2;
				double c1 = r*gsl_matrix_get(S,j,0)*dt/(2*ds) - rho*dt/(ds*ds);
				double c2 = 1 + r*dt + 2*rho * dt/(ds*ds);
				double c3 = -r*gsl_matrix_get(S,j,0)*dt/(2*ds) - rho*dt/(ds*ds);
				gsl_matrix_set(cf,j,0, c1);
				gsl_matrix_set(cf,j,1, c2);
				gsl_matrix_set(cf,j,2, c3);
			}
		}
		break;
	}
	return cf;
}

gsl_matrix* GetFreeCoefficientsColumn(gsl_matrix_view vView, gsl_matrix* S, const double& t, const size_t& n, const size_t& N, const size_t& M,
		const double& r, const double& sd, const Scheme& scheme)
{
	double dt = (double)t/n;
	switch (scheme)
	{
		//TODO
		case CrankNicolsonIm:
		{
			gsl_matrix* m = gsl_matrix_alloc(M,1);
			gsl_matrix* Vtemp = &vView.matrix;
			for (size_t i = 1; i < M - 1; ++i)
			{
				double ds = gsl_matrix_get(S, 1, 0) - gsl_matrix_get(S,0,0);
				double value = gsl_matrix_get(Vtemp, i, 0) + r * gsl_matrix_get(S, i, 0) * dt / (4 * ds) * (gsl_matrix_get(Vtemp, i + 1, 0) - gsl_matrix_get(Vtemp, i - 1, 0)) +
						sd*sd*gsl_matrix_get(S, i, 0)*gsl_matrix_get(S, i, 0)*dt/(4*ds*ds)*(gsl_matrix_get(Vtemp, i + 1, 0) - 2*gsl_matrix_get(Vtemp, i , 0) + gsl_matrix_get(Vtemp, i - 1, 0)) -
						r*dt*gsl_matrix_get(Vtemp, i, 0)/2;
				gsl_matrix_set(m, i, 0, value);
			}
			gsl_matrix_set(m, 0, 0, gsl_matrix_get(Vtemp,0,0));
			gsl_matrix_set(m, M - 1, 0, gsl_matrix_get(Vtemp, M - 1, 0));
			return m;
		}
		//TODO
		case CrankNicolsonDuffyIm:
		{
			gsl_matrix* m = gsl_matrix_alloc(M,1);
			gsl_matrix* Vtemp = &vView.matrix;
			for (size_t i = 1; i < M - 1; ++i)
			{
				double ds = gsl_matrix_get(S, 1, 0) - gsl_matrix_get(S,0,0);
				double mu = r*gsl_matrix_get(S,i,0);
				double sigma = 0.5* sd*sd*gsl_matrix_get(S,i,0)*gsl_matrix_get(S,i,0);
				double rho = mu*ds*0.5*1/(tanh(mu*ds/(2*sigma)));
				double value = gsl_matrix_get(Vtemp, i, 0) + r * gsl_matrix_get(S, i, 0) * dt / (4 * ds) * (gsl_matrix_get(Vtemp, i + 1, 0) - gsl_matrix_get(Vtemp, i - 1, 0)) +
						rho*dt/(2*ds*ds)*(gsl_matrix_get(Vtemp, i + 1, 0) - 2*gsl_matrix_get(Vtemp, i , 0) + gsl_matrix_get(Vtemp, i - 1, 0)) -
						r*dt*gsl_matrix_get(Vtemp, i, 0)/2;
				gsl_matrix_set(m, i, 0, value);
			}
			gsl_matrix_set(m, 0, 0, gsl_matrix_get(Vtemp,0,0));
			gsl_matrix_set(m, M - 1, 0, gsl_matrix_get(Vtemp, M - 1, 0));
			return m;
		}
		default:
		{
			gsl_matrix* m = gsl_matrix_alloc(M-2,1);
			gsl_matrix* Vtemp = &vView.matrix;
			size_t s1 = gsl_matrix_get_size1(Vtemp);
			for (size_t i = 0; i < s1; ++i)
			{
				gsl_matrix_set(m, i, 0, gsl_matrix_get(Vtemp, i, 0));
			}
			return m;
		}
	}
}

void FiniteDifferenceSolver::RenewKnockoutZone(const double& rebate, const size_t& column, gsl_matrix* V, gsl_matrix* S, const OptionTypes& option, const bool& put)
{
	const BarrierOption* bar = static_cast<const BarrierOption*>(&option);
	double upperBarrier = bar->UpperBarrier();
	vector<double> assetPrices = GslColumnToVector1(0, S);
	vector<double>::iterator it = lower_bound(assetPrices.begin(), assetPrices.end(), upperBarrier);
	int index = it - assetPrices.begin();
	if (put)
	{
		for (size_t j = 0; j < (size_t)index; ++j)
		{
			gsl_matrix_set(V, j, column, rebate);
		}
	}
	else
	{
		for (size_t j = (size_t)index; j < assetPrices.size(); ++j)
		{
			gsl_matrix_set(V, j, column, rebate);
		}
	}
}

void FiniteDifferenceSolver::CalculateValues(gsl_matrix* S, gsl_matrix* V, gsl_matrix* add, gsl_matrix* A, const size_t& N, const size_t& M, const double& t, const double& r, const double& sd, const double& rebate,
		gsl_matrix* (FiniteDifferenceSolver::* solver)(gsl_matrix*, gsl_matrix*,const size_t&, const size_t&), const OptionTypes& option, const Scheme& scheme, const bool& put)
{
	OptionType type = option.GetType();
	size_t lineStart = 0;
	size_t lineFinish = scheme == CrankNicolsonIm || scheme == CrankNicolsonDuffyIm ?
			M-1 : M-3;
	switch (type)
	{
		case KnockOut:
		{
			/*const BarrierOption* bar = static_cast<const BarrierOption*>(&option);
			double upperBarrier = bar->UpperBarrier();
			vector<double> assetPrices = GslColumnToVector1(0, S);
			vector<double>::iterator it = lower_bound(assetPrices.begin(), assetPrices.end(), upperBarrier);
			int index = it - assetPrices.begin();
			if (put)
			{
				lineStart = index;
			}
			else
			{
				if (assetPrices.end() == it)
					return;
				lineFinish = index;
			}*/
		}
		break;
		default:
			break;
	}

	if (put && lineStart >= lineFinish)
		return;
	//TODO
	//Profile memory leaks
	/*for (int i = N-1; i >= 1; --i)
	{
		size_t matrixSize = scheme == CrankNicolsonIm || scheme == CrankNicolsonDuffyIm ?
				M : M - 2;
		size_t row = scheme == CrankNicolsonIm || scheme == CrankNicolsonDuffyIm ?
				0 : 1;
		gsl_matrix_view VView = gsl_matrix_submatrix(V, row, i, matrixSize, 1);
		gsl_matrix* v = GetFreeCoefficientsColumn(VView, S, t, N - 1, N, M, r, sd, scheme);
		gsl_matrix_view vView = gsl_matrix_submatrix(v,0,0, matrixSize,1);
		gsl_matrix_view addView = gsl_matrix_submatrix(add,0,i-1,M-2,1);
		gsl_matrix* subResult = scheme == CrankNicolsonIm || scheme == CrankNicolsonDuffyIm ?
				v :
				gsl_matrix_add_separate(&vView.matrix, &addView.matrix);

		gsl_matrix* result = (this->*solver)(A, subResult, lineStart, lineFinish);
		//MatrixOutput(result);
		if (scheme != CrankNicolsonIm && scheme != CrankNicolsonDuffyIm)
			gsl_matrix_free(subResult);
		size_t resultSize = gsl_matrix_get_size1(result);
		size_t startPosition = put ? matrixSize - resultSize : 0;
		for (size_t j = 0; j <= resultSize - 1; ++j)
		{
			double val = gsl_matrix_get(result, j, 0);
			if (scheme == CrankNicolsonIm || scheme == CrankNicolsonDuffyIm)
			{
				gsl_matrix_set(V, j + startPosition, i - 1, val);
			}
			else
			{
				gsl_matrix_set(V, j + startPosition + 1, i-1, val);
			}

		}*/
		for (int i = N-1; i >= 1; --i)
		{
			size_t matrixSize = scheme == CrankNicolsonIm || scheme == CrankNicolsonDuffyIm ?
					M : M - 2;
			size_t row = scheme == CrankNicolsonIm || scheme == CrankNicolsonDuffyIm ?
					0 : 1;
			gsl_matrix_view VView = gsl_matrix_submatrix(V, row, i, matrixSize, 1);
			gsl_matrix* v = GetFreeCoefficientsColumn(VView, S, t, N - 1, N, M, r, sd, scheme);
			gsl_matrix_view vView = gsl_matrix_submatrix(v,0,0, matrixSize,1);
			gsl_matrix_view addView = gsl_matrix_submatrix(add,0,i-1,M-2,1);
			gsl_matrix* subResult = scheme == CrankNicolsonIm || scheme == CrankNicolsonDuffyIm ?
					v :
					gsl_matrix_add_separate(&vView.matrix, &addView.matrix);

			gsl_matrix* result = (this->*solver)(A, subResult, lineStart, lineFinish);
			//MatrixOutput(result);
			if (scheme != CrankNicolsonIm && scheme != CrankNicolsonDuffyIm)
				gsl_matrix_free(subResult);
			size_t resultSize = gsl_matrix_get_size1(result);
			for (size_t j = 0; j <= resultSize - 1; ++j)
			{
				double val = gsl_matrix_get(result, j, 0);
				if (scheme == CrankNicolsonIm || scheme == CrankNicolsonDuffyIm)
				{
					gsl_matrix_set(V, j, i - 1, val);
				}
				else
				{
					gsl_matrix_set(V, j + 1, i-1, val);
				}

			}
			if (type == KnockOut)
			{
				RenewKnockoutZone(rebate, i - 1, V, S, option, put);
			}
			gsl_matrix_view vCheck = gsl_matrix_submatrix(V, 0, i-1, M, 1);
			//MatrixOutput(&vCheck.matrix);
			gsl_matrix_free(v);
			gsl_matrix_free(result);
		}
}

gsl_matrix* FiniteDifferenceSolver::DeclareMatrixCoefficients(gsl_matrix* cf, const size_t& M, const Scheme& scheme)
{
	size_t matrixSize = scheme == CrankNicolsonIm || scheme == CrankNicolsonDuffyIm ?
			M : M - 2;
	vector<double> Arow(matrixSize, 0);
	vector< vector<double> > Avalues(matrixSize, Arow);
	gsl_matrix* A = DeclareGslMatrix(Avalues);
	size_t k = -1;
	size_t shift = matrixSize == M ? 0 : 1;
	for (size_t j = 1; j < matrixSize - 1; ++j)
	{
		gsl_matrix_set(A, j, k+1, gsl_matrix_get(cf, j+shift, 0));
		gsl_matrix_set(A, j, k+2, gsl_matrix_get(cf, j+shift, 1));
		gsl_matrix_set(A, j, k+3, gsl_matrix_get(cf, j+shift, 2));
		++k;
	}
	switch (scheme)
	{
		case CrankNicolsonIm:
		{
			CrankNicolson:
			gsl_matrix_set(A, 0, 0, gsl_matrix_get(cf, 0, 1));
			gsl_matrix_set(A, matrixSize - 1, matrixSize - 1, gsl_matrix_get(cf, matrixSize - 1, 1));
		}
		break;
		case CrankNicolsonDuffyIm:
		{
			goto CrankNicolson;
		}
		break;
		default:
		{
			gsl_matrix_set(A, 0, 0, gsl_matrix_get(cf, 0, 1));
			gsl_matrix_set(A, 0 ,1, gsl_matrix_get(cf, 0, 2));
			gsl_matrix_set(A, matrixSize - 1, matrixSize - 2, gsl_matrix_get(cf, matrixSize - 1, 0));
			gsl_matrix_set(A, matrixSize - 1, matrixSize - 1, gsl_matrix_get(cf, matrixSize - 1, 1));
		}
		break;
	}
	return A;

}

//Finite difference implicit scheme for pricing an exotic put option with a help of a specific SLAE solver
// s0 is the maximum asset price
// K is the strike
// t is the time
// r is the risk-free rate
// y is the dividend yield
// sd is the historical volatility
// n is the number of steps
// solver is the function responsible for the method of solving the equation (by default, Gauss elimination LU)
tr1::tuple<double, vector<double>, vector< vector<double> >, vector< vector<double> >,vector< vector<double> > > FiniteDifferenceSolver::FiniteDifferenceImplicitSolver(const double& s0, const double& K, const double& t, const double& r,
		const double& y, const double& sd, const size_t& n, gsl_matrix* (FiniteDifferenceSolver::* solver)(gsl_matrix*, gsl_matrix*,const size_t&, const size_t&),const OptionTypes& option,const bool& put) {
	m_valueToCompare = s0;
	size_t N = n+1;
	size_t M = n*2 + 1;
	gsl_matrix* S = GenerateAssetGrid(0, 2*s0, n, N, M, t, s0, K, r, put);
	gsl_matrix* V = GenerateValueGrid(S, n, N, M, t, s0, K, r, 0, option, put);

	gsl_matrix* cf = GenerateCoefficients(S, M, t, n, sd, r, y, Implicit);
	//MatrixOutput(cf);
	vector<double> Arow(M-2, 0);
	vector< vector<double> > Avalues(M-2, Arow);
	gsl_matrix* A = DeclareGslMatrix(Avalues);
	gsl_matrix_set(A, 0, 0, gsl_matrix_get(cf, 1, 1));
	gsl_matrix_set(A, 0 ,1, gsl_matrix_get(cf, 1, 2));
	gsl_matrix_set(A, M-3, M-4, gsl_matrix_get(cf, M-2,0));
	gsl_matrix_set(A, M-3, M-3, gsl_matrix_get(cf, M-2,1));

	size_t k = -1;
	for (size_t j = 1; j < M-3; ++j)
	{
		gsl_matrix_set(A, j, k+1, gsl_matrix_get(cf, j+1, 0));
		gsl_matrix_set(A, j, k+2, gsl_matrix_get(cf, j+1, 1));
		gsl_matrix_set(A, j, k+3, gsl_matrix_get(cf, j+1, 2));
		++k;
	}

	vector<double> addRow(N-1,0);
	vector< vector<double> > addMatrix(M-2, addRow);
	gsl_matrix* add = DeclareGslMatrix(addMatrix);

	for (int i = N-2; i >= 0; --i)
	{
		double val1 = -gsl_matrix_get(cf,1,0)*gsl_matrix_get(V,0,i);
		double val2 = gsl_matrix_get(cf,M-2,2)*gsl_matrix_get(V,M-2,N-3);
		gsl_matrix_set(add, 0, i, val1);
		gsl_matrix_set(add, M-3, i, val2);
	}

	//MatrixOutput(add);
	//TODO
	//customize to other options
	for (int i = N-1; i >= 1; --i)
	{
		gsl_matrix_view vView = gsl_matrix_submatrix(V,1,i,M-2,1);
		gsl_matrix_view addView = gsl_matrix_submatrix(add,0,i-1,M-2,1);
		gsl_matrix* subResult = gsl_matrix_add_separate(&vView.matrix, &addView.matrix);
		gsl_matrix* result = (this->*solver)(A, subResult, 0, M - 3);
		gsl_matrix_free(subResult);
		for (size_t j = 1; j <= M-2; ++j)
		{
			double val = gsl_matrix_get(result, j-1, 0);
			gsl_matrix_set(V, j, i-1, val);
		}
		gsl_matrix_free(result);
	}
	double result = gsl_matrix_get(V,n,0);
	//gsl_matrix_view view = gsl_matrix_submatrix(V,0,0,M,1);
	tr1::tuple<double, vector<double>, vector< vector<double> >, vector< vector<double> >,vector< vector<double> > > outputTuple;
	outputTuple = std::tr1::make_tuple(result, GslColumnToVector(0, V), GslMatrixToVector(S, false), GslMatrixToVector(V, false), GslMatrixToVector(cf, false));
	gsl_matrix_free(V);
	gsl_matrix_free(S);
	gsl_matrix_free(A);
	gsl_matrix_free(add);
	gsl_matrix_free(cf);
	return outputTuple;
}

tr1::tuple<double, vector<double>, vector< vector<double> >, vector< vector<double> >,vector< vector<double> > > FiniteDifferenceSolver::FiniteDifferenceImplicitDuffySolver(const double& s0, const double& K, const double& t, const double& r,
		const double& y, const double& sd, const double& rebate, const size_t& n, gsl_matrix* (FiniteDifferenceSolver::* solver)(gsl_matrix*, gsl_matrix*,const size_t&, const size_t&),
		const OptionTypes& option, const Scheme& scheme, const bool& put)
{
	m_valueToCompare = s0;
	size_t N = n+1;
	size_t M = n*2 + 1;
	gsl_matrix* S = GenerateAssetGrid(0, 2*s0, n, N, M, t, s0, K, r, put);
	gsl_matrix* V = GenerateValueGrid(S, n, N, M, t, s0, K, r, 0, option, put);
	//cout << "S" << endl;
	//MatrixOutput(S);
	gsl_matrix* cf = GenerateCoefficients(S, M, t, n, sd, r, y, scheme);
	//cout << "cf" << endl;
	//MatrixOutput(cf);
	gsl_matrix* A = DeclareMatrixCoefficients(cf, M, scheme);

	gsl_matrix* add = GenerateAddCoefficients(V, N, M, cf, scheme);
	//cout << "add" << endl;
	//MatrixOutput(add);
	CalculateValues(S, V,add,A,N,M, t, r, sd, rebate, solver,option, scheme, put);
	//cout << "V" << endl;
	//MatrixOutput(V);
	vector<double> s = GslColumnToVector1(0, S);
	vector<double>::iterator it = lower_bound(s.begin(), s.end(), s0);
	int index = it - s.begin();
	double result = gsl_matrix_get(V,index,0);
	if (it == s.end())
		result = 0;
	//gsl_matrix_view view = gsl_matrix_submatrix(V,0,0,M,1);
	tr1::tuple<double, vector<double>, vector< vector<double> >, vector< vector<double> >,vector< vector<double> > > outputTuple;
	outputTuple = std::tr1::make_tuple(result, GslColumnToVector(0, V), GslMatrixToVector(S, false), GslMatrixToVector(V, false), GslMatrixToVector(cf, false));
	gsl_matrix_free(V);
	gsl_matrix_free(S);
	gsl_matrix_free(A);
	gsl_matrix_free(add);
	gsl_matrix_free(cf);
	return outputTuple;
}

double FiniteDifferenceSolver::FiniteDifferenceImplicitLogSolver(const double& s0, const double& K, const double& t, const double& r,
				const double& y, const double& sd, const size_t& n, gsl_matrix* (FiniteDifferenceSolver::* solver)(gsl_matrix*, gsl_matrix*,const size_t&, const size_t&),
				const OptionTypes& option,const bool& put)
{
	m_valueToCompare = s0;
	size_t N = n+1;
	size_t M = n*2 + 1;
	double dT = (double)t/n;
	double dlogS = sd * sqrt(2* dT);
	gsl_matrix* logS = GenerateAssetGrid(-dlogS*n, dlogS*n, n, N, M, t, s0, K, r, put);
	gsl_matrix* S = ExponentMatrix(logS);
	gsl_matrix_free(logS);
	gsl_matrix_scale(S, s0);
	gsl_matrix* V = GenerateValueGrid(S, n, N, M, t, s0, K, r, 0, option, put);
	gsl_matrix* cf = GenerateCoefficients(S, M, t, n, sd, r, y, ImplicitLog);

	vector<double> Arow(M-2, 0);
	vector< vector<double> > Avalues(M-2, Arow);
	gsl_matrix* A = DeclareGslMatrix(Avalues);
	gsl_matrix_set(A, 0, 0, gsl_matrix_get(cf, 1, 1));
	gsl_matrix_set(A, 0 ,1, gsl_matrix_get(cf, 1, 2));
	gsl_matrix_set(A, M-3, M-4, gsl_matrix_get(cf, M-2,0));
	gsl_matrix_set(A, M-3, M-3, gsl_matrix_get(cf, M-2,1));

	size_t k = -1;
	for (size_t j = 1; j < M-3; ++j)
	{
		gsl_matrix_set(A, j, k+1, gsl_matrix_get(cf, j+1, 0));
		gsl_matrix_set(A, j, k+2, gsl_matrix_get(cf, j+1, 1));
		gsl_matrix_set(A, j, k+3, gsl_matrix_get(cf, j+1, 2));
		++k;
	}
	vector<double> addRow(N-1,0);
	vector< vector<double> > addMatrix(M-2, addRow);
	gsl_matrix* add = DeclareGslMatrix(addMatrix);

	for (int i = N-2; i >= 0; --i)
	{
		double val1 = -gsl_matrix_get(cf,1,0)*gsl_matrix_get(V,0,i);
		double val2 = gsl_matrix_get(cf,M-2,2)*gsl_matrix_get(V,M-2,N-3);
		gsl_matrix_set(add, 0, i, val1);
		gsl_matrix_set(add, M-3, i, val2);
	}


	for (int i = N-1; i >= 1; --i)
	{
		gsl_matrix_view vView = gsl_matrix_submatrix(V,1,i,M-2,1);
		gsl_matrix_view addView = gsl_matrix_submatrix(add,0,i-1,M-2,1);
		gsl_matrix* subResult = gsl_matrix_add_separate(&vView.matrix, &addView.matrix);
		gsl_matrix* result = (this->*solver)(A, subResult, 0, M-3);
		gsl_matrix_free(subResult);
		for (size_t j = 1; j <= M-2; ++j)
		{
			double val = gsl_matrix_get(result, j-1, 0);
			gsl_matrix_set(V, j, i-1, val);
		}
		gsl_matrix_free(result);
	}
	vector<double> s = GslColumnToVector(0, S);
	vector<double>::iterator it = min_element(s.begin(), s.end(), optionPriceComparatorLess);
	int index = it - s.begin();
	double result = gsl_matrix_get(V,index,0);

	gsl_matrix_free(V);
	gsl_matrix_free(S);
	gsl_matrix_free(A);
	gsl_matrix_free(add);
	gsl_matrix_free(cf);
	return result;
}

//TODO
//Add dividend yield
double FiniteDifferenceSolver::FiniteDifferenceExplicitLogSolver(const double& s0, const double& K, const double& t, const double& r,
				const double& y, const double& sd, const size_t& n,
				const OptionTypes& option,const bool& put)
{
	m_valueToCompare = 0;
	size_t N = n+1;
	size_t M = n*2 + 1;
	double dT = (double)t/n;
	double dlogS = sd * sqrt(2* dT);
	gsl_matrix* logS = GenerateAssetGrid(-dlogS*n, dlogS*n, n, N, M, t, s0, K, r, put);
	gsl_matrix* S = ExponentMatrix(logS);
	gsl_matrix_free(logS);
	gsl_matrix_scale(S, s0);
	gsl_matrix* V = GenerateValueGrid(S, n, N, M, t, s0, K, r, 0, option, put);
	gsl_matrix* cf = GenerateCoefficients(S, M, t, n, sd, r, y, ExplicitLog);
	vector<double> Arow(M, 0);
	vector< vector<double> > Avalues(M-2, Arow);
	gsl_matrix* A = DeclareGslMatrix(Avalues);

	size_t k = -1;
	for (size_t j = 0; j < M-2; ++j)
	{
		gsl_matrix_set(A, j, k+1, gsl_matrix_get(cf, j, 0));
		gsl_matrix_set(A, j, k+2, gsl_matrix_get(cf, j, 1));
		gsl_matrix_set(A, j, k+3, gsl_matrix_get(cf, j, 2));
		++k;
	}

	for (int i = N-1; i >= 1; --i)
	{
		gsl_matrix_view vView = gsl_matrix_submatrix(V,0,i,M,1);
		gsl_matrix* subResult = MultiplyMatrix(A, &vView.matrix, false, false);
		for (size_t j = 1; j <= M-2; ++j)
		{
			double val = gsl_matrix_get(subResult, j-1, 0);
			gsl_matrix_set(V, j, i-1, val);
		}
		gsl_matrix_free(subResult);
	}
	vector<double> s = GslColumnToVector(0, S);
	vector<double>::iterator it = min_element(s.begin(), s.end(), optionPriceComparatorLess);
	int index = it - s.begin();
	double result = gsl_matrix_get(V,index,0);
	gsl_matrix_free(V);
	gsl_matrix_free(S);
	gsl_matrix_free(A);
	gsl_matrix_free(cf);
	return result;
}

FiniteDifferenceSolver::~FiniteDifferenceSolver() {
	// TODO Auto-generated destructor stub
	if (m_invertSolverMatrix != 0)
		gsl_matrix_free(m_invertSolverMatrix);
}

