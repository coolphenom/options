/*
 * OptionTypes.cpp
 *
 *  Created on: Jun 26, 2019
 *      Author: pavlotkachenko
 */

#include "OptionTypes.h"

OptionTypes::OptionTypes() {
	// TODO Auto-generated constructor stub

}

OptionTypes::~OptionTypes() {
	// TODO Auto-generated destructor stub
}


VanillaOption::VanillaOption() {
	// TODO Auto-generated constructor stub
	m_type = Vanilla;
}

VanillaOption::~VanillaOption() {
	// TODO Auto-generated destructor stub
}


BarrierOption::BarrierOption(const double& upperBarrier, const double& lowerBarrier, const double& rebate) {
	// TODO Auto-generated constructor stub
	m_upperBarrier = upperBarrier;
	m_lowerBarrier = lowerBarrier;
	m_rebate = rebate;
	if (m_upperBarrier > 0 && m_lowerBarrier == 0)
	{
		m_type = KnockOut;
	}
	else if (m_lowerBarrier > 0 && m_upperBarrier == 0)
	{
		m_type = KnockIn;
	}
}

const double& BarrierOption::LowerBarrier() const
{
	return m_lowerBarrier;
}
const double& BarrierOption::UpperBarrier() const
{
	return m_upperBarrier;
}

BarrierOption::~BarrierOption() {
	// TODO Auto-generated destructor stub
}
