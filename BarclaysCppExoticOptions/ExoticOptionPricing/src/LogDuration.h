/*
 * LogDuration.h
 *
 *  Created on: Jun 14, 2019
 *      Author: pavlotkachenko
 */

#ifndef LOGDURATION_H_
#define LOGDURATION_H_

#include <iostream>
#include <string>
#include <ctime>

#define UNIQ_ID_IMPL(lineno) _a_local_var##lineno
#define UNIQ_ID(lineno) UNIQ_ID_IMPL(lineno)

class LogDuration {
public:
	explicit LogDuration(const std::string& msg = "");
	virtual ~LogDuration();
private:
	std::string message;
	clock_t start;
};

#define LOG_DURATION(message) \
	LogDuration UNIQ_ID(__LINE__)(message)

#endif /* LOGDURATION_H_ */
