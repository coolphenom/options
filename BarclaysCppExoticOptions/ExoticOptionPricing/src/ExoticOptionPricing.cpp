//============================================================================
// Name        : ExoticOptionPricing.cpp
// Author      : Pavlo Tkachenko
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <stdexcept>
#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_permutation.h>

#include "LogDuration.h"
#include "FiniteDifferenceSolver.h"

using namespace std;

int main() {
/*	{
		LOG_DURATION("LU decomposition");
		FiniteDifferenceSolver s;
		double price = s.FiniteDifferenceImplicitSolver(30,30,300, 0.01/365,0,0.02, 400, &FiniteDifferenceSolver::LUSolver);
		cout <<  price << endl;
	}
	{
		LOG_DURATION("Tridiagonal");
		FiniteDifferenceSolver s;
		double price = s.FiniteDifferenceImplicitSolver(30,30,300, 0.01/365,0,0.02, 400, &FiniteDifferenceSolver::TridiagonalSolver);
		cout <<  price << endl;
	}*/
	/*{
		LOG_DURATION("Explicit Log");
		FiniteDifferenceSolver s;
		double price = s.FiniteDifferenceExplicitLogSolver(30,30,300, 0.01/365,0,0.02, 20);
		cout <<  price << endl;
	}*/
//	{
//		LOG_DURATION("LU decomposition");
//		FiniteDifferenceSolver s;
//		double price = s.FiniteDifferenceImplicitLogSolver(30,30,300, 0.01/365,0,0.02, 400, &FiniteDifferenceSolver::LUSolver);
//		cout <<  price << endl;
//	}
	/*{
		LOG_DURATION("Tridiagonal");
		FiniteDifferenceSolver s;
		double price = s.FiniteDifferenceImplicitSolver(30,30,300, 0.01/365,0,0.02, 20, &FiniteDifferenceSolver::TridiagonalSolver,
				BarrierOption(22,0));
		cout <<  price << endl;
	}*/
	{
		LOG_DURATION("Triagonal");
		FiniteDifferenceSolver s;
		//double price = s.FiniteDifferenceImplicitDuffySolver(30.0,30.0, 300, 0.01/365,0,0.02, 100, &FiniteDifferenceSolver::TridiagonalSolver,
		//		VanillaOption(), Implicit);
		//cout <<  price << endl;
//		for (double i = 1; i < 60; i+= 0.25)
//		{
//			double price = s.FiniteDifferenceImplicitDuffySolver(i, 30.0, 300, 0.01/365,0,0.02, 100, &FiniteDifferenceSolver::TridiagonalSolver,
//							BarrierOption(22,0,0), CrankNicolsonIm);
//			cout <<  price << endl;
//		}
//		cout << "----------" << endl;
//		for (double i = 1; i < 60; i+= 0.25)
//		{
	//		double price = s.FiniteDifferenceImplicitDuffySolver(30, 30.0, 300, 0.01/365,0,0.02, 0, 100, &FiniteDifferenceSolver::LUSolver,
		//					BarrierOption(22,0,0), CrankNicolsonDuffyIm);
		//	cout <<  price << endl;
//		}
//		cout << "----------" << endl;
		//for (double i = 1; i < 200; i+=1)
		//{
		//	double price = s.FiniteDifferenceImplicitDuffySolver(100, 100, 0.5, 0.05, 0, 0.2, 10, 2000, &FiniteDifferenceSolver::TridiagonalSolver,
		//					BarrierOption(110,0,0), DuffyIm, false);
		//	cout <<  price << endl;
		//}
			//double price = s.FiniteDifferenceImplicitDuffySolver(30, 30, 10, 0.05, 0, 0.2, 0, 5, &FiniteDifferenceSolver::LUSolver,
			//							BarrierOption(22,0,0), DuffyIm, true);
			//		cout <<  price << endl;

	}
	return 0;
}
