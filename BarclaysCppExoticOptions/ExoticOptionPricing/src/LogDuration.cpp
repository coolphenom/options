/*
 * LogDuration.cpp
 *
 *  Created on: Jun 14, 2019
 *      Author: pavlotkachenko
 */

#include "LogDuration.h"

LogDuration::LogDuration(const std::string& msg) :
						message(msg + ": "),
						start(clock())
{
	// TODO Auto-generated constructor stub

}

LogDuration::~LogDuration() {
	clock_t finish = clock();
	float duration = ((float)(finish - start))/CLOCKS_PER_SEC;
	std::cerr << message << duration  << " sec"<< std::endl;
	// TODO Auto-generated destructor stub
}

