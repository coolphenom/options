/*
 * OptionTypes.h
 *
 *  Created on: Jun 26, 2019
 *      Author: pavlotkachenko
 */

#ifndef OPTIONTYPES_H_
#define OPTIONTYPES_H_

enum OptionType
{
	Vanilla,
	KnockOut,
	KnockIn
};

class OptionTypes {
public:
	explicit OptionTypes();
	const OptionType GetType() const {return m_type;}
	virtual ~OptionTypes();
protected:
	OptionType m_type;
};

class VanillaOption : public OptionTypes
{
public:
	explicit VanillaOption();
	virtual ~VanillaOption();
};

class BarrierOption : public OptionTypes
{
public:
	explicit BarrierOption(const double& upperBarrier, const double& lowerBarrier, const double& rebate);
	const double& LowerBarrier() const;
	const double& UpperBarrier() const;
	const double& Rebate() const;
	virtual ~BarrierOption();
private:
	double m_lowerBarrier;
	double m_upperBarrier;
	double m_rebate;
};

#endif /* OPTIONTYPES_H_ */
