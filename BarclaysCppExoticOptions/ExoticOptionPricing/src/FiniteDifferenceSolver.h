/*
 * FiniteDifferenceSolver.h
 *
 *  Created on: Jun 18, 2019
 *      Author: pavlotkachenko
 */

#ifndef FINITEDIFFERENCESOLVER_H_
#define FINITEDIFFERENCESOLVER_H_

#include <iostream>
#include <vector>
#include <stdexcept>
#include <tr1/tuple>
#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_permutation.h>

#include "LogDuration.h"
#include "OptionTypes.h"

enum Scheme
{
	Implicit,
	ExplicitLog,
	ImplicitLog,
	CrankNicolsonIm,
	CrankNicolsonDuffyIm,
	DuffyIm
};

class FiniteDifferenceSolver {
public:
	explicit FiniteDifferenceSolver();
	gsl_matrix* LUSolver(gsl_matrix* A, gsl_matrix* b, const size_t& indStart, const size_t& indFinish);
	gsl_matrix* TridiagonalSolver(gsl_matrix* A, gsl_matrix* b, const size_t& indStart, const size_t& indFinish);
	std::tr1::tuple<double, std::vector<double>, std::vector< std::vector<double> >, std::vector< std::vector<double> >,std::vector< std::vector<double> > > FiniteDifferenceImplicitSolver(const double& s0, const double& K, const double& t, const double& r,
			const double& y, const double& sd, const size_t& n, gsl_matrix* (FiniteDifferenceSolver::* solver)(gsl_matrix*, gsl_matrix*, const size_t& indStart, const size_t& indFinish),
			const OptionTypes& option, const bool& put = true);
	double FiniteDifferenceExplicitLogSolver(const double& s0, const double& K, const double& t, const double& r,
				const double& y, const double& sd, const size_t& n,
				const OptionTypes& option, const bool& put = true);
	double FiniteDifferenceImplicitLogSolver(const double& s0, const double& K, const double& t, const double& r,
				const double& y, const double& sd, const size_t& n, gsl_matrix* (FiniteDifferenceSolver::* solver)(gsl_matrix*, gsl_matrix*,const size_t& indStart, const size_t& indFinish),
				const OptionTypes& option, const bool& put = true);
	std::tr1::tuple<double, std::vector<double>, std::vector< std::vector<double> >, std::vector< std::vector<double> >,std::vector< std::vector<double> > > FiniteDifferenceImplicitDuffySolver(const double& s0, const double& K, const double& t, const double& r,
			const double& y, const double& sd, const double& rebate, const size_t& n, gsl_matrix* (FiniteDifferenceSolver::* solver)(gsl_matrix*, gsl_matrix*,const size_t& indStart, const size_t& indFinish),
			const OptionTypes& option, const Scheme& scheme, const bool& put = true);
	virtual ~FiniteDifferenceSolver();
private:
	gsl_matrix* GenerateAddCoefficients(gsl_matrix* V, const size_t& N, const size_t& M,
			gsl_matrix* cf, const Scheme& scheme);
	void CalculateValues(gsl_matrix* S, gsl_matrix* V, gsl_matrix* add, gsl_matrix* A, const size_t& N, const size_t& M, const double& t, const double& r, const double& sd,
			const double& rebate,
			gsl_matrix* (FiniteDifferenceSolver::* solver)(gsl_matrix*, gsl_matrix*,const size_t& indStart, const size_t& indFinish),
			const OptionTypes& option, const Scheme& scheme, const bool& put);
	std::pair<gsl_matrix*, gsl_matrix*> GenerateGrid(const size_t& n, const size_t& N, const size_t& M,
			const double& t, const double& s0, const double& K, const double& r, const bool& put);
	gsl_matrix* GenerateCoefficients(gsl_matrix* S, const size_t& M, const double& t, const size_t& n,
			const double& sd, const double& r, const double& y, const Scheme& scheme);
	gsl_matrix* DeclareMatrixCoefficients(gsl_matrix* cf, const size_t& M, const Scheme& scheme);
	gsl_matrix* GenerateAssetGrid(const double& start, const double& finish, const size_t& n, const size_t& N, const size_t& M, const double& t,
			const double& s0, const double& K, const double& r, const bool& put);
	gsl_matrix* GenerateValueGrid(gsl_matrix* S, const size_t& n, const size_t& N, const size_t& M, const double& t,
			const double& s0, const double& K, const double& r, const double& rebate, const OptionTypes& option, const bool& put);

	void RenewKnockoutZone(const double& rebate, const size_t& column, gsl_matrix* V, gsl_matrix* S, const OptionTypes& option, const bool& put);

	static bool optionPriceComparatorLess(double a, double b);
	static bool optionPriceComparatorMore(double a, double b);
	gsl_matrix* m_invertSolverMatrix;
	static double m_valueToCompare;
};

#endif /* FINITEDIFFERENCESOLVER_H_ */
